#!/usr/bin/perl
#check migration diff for weird entries (where non-weird means cache urls simply changed uuids)
use strict;

my $diff = shift;
open(IN,"<$diff");
my $current_file = "";
my $should_print;
my @diff_lines;
my %counts;
$counts{'>'}=0;
$counts{'<'}=0;
while(my $line = <IN>)
{
	chomp($line);
	if($line !~ /^[><]/ && $line !~ /^diff/)
	{
		push(@diff_lines,$line);
	}
	elsif($line =~ /^diff/)
	{
		if($counts{'>'} != $counts{'<'})
		{
			print "$current_file\n";
			foreach my $d (@diff_lines)
			{
				print "$d\n";
			}
		}
		undef(@diff_lines);
		$should_print=undef;
		$current_file = $line;
		$counts{'>'}=0;
		$counts{'<'}=0;
	}
	else
	{
		push(@diff_lines,$line);
		$counts{substr($line,0,1)}++;
	}
}
		if($counts{'>'} != $counts{'<'})
		{
			print "$current_file\n";
			foreach my $d (@diff_lines)
			{
				print "$d\n";
			}
		}
close(IN);
